// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

import { query } from '../../lib/db';

type Data = {
  name: string
}

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const { title, name, email, password } = req.body;
    try {
        if (!title || !name || !email || !password) {
            return res
            .status(400)
            .json({ message: '`title`, `name`, `email`, `password`  are required' })
        }
        const results = await query(
            `
            INSERT INTO l_users (title, name, email, password)
            VALUES (?, ?, ?, ?)
            `,
            [title, name, email, password]
          )
          return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
    // res.status(200).json({ name: 'John Doe' })
}