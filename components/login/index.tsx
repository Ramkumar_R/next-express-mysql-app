import React, { useState } from 'react';
import { Container, Form, Button, Row } from 'react-bootstrap';

import { execute } from '../../helpers/ApiInterface';

export default function Login () {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const handleSubmit = async (event: any) => {
        event.preventDefault();
        if (email && password) {
            console.log({email, password});
            const resp = await execute<any>(
                'POST',
                'api/login',
                {
                    email,
                    password,
                },
            );
            console.log(resp);
        }
    };

    return (
        <>
            <div>
                <Container>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter Password" onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>
                        <Form.Group as={Row} className="d-flex justify-content-center">
                            <Button variant="primary" type="submit">Login</Button>
                        </Form.Group>
                    </Form>
                </Container>
                
            </div>
        </>
    );
}