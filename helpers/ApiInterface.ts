import axios, { AxiosTransformer } from 'axios';

type RestAPIMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

export const execute = <T>(
    method: RestAPIMethod, path: string, body?: any,
    transformResponse?: AxiosTransformer,
  ) => {
    let headers: any = {
      'Content-Type': 'application/json',
    };
  
    headers = {
      ...headers,
    };
  
    return axios.request<T>({
      method,
      headers,
      url: `/${path}`,
      data: JSON.stringify(body),
      transformResponse,
    });
  };
  
  export default {
    execute,
  };