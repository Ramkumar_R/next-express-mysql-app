// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

import { query } from '../../lib/db';

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const { email, password } = req.body;
    try {
        if (!email || !password) {
            return res
            .status(400)
            .json({ message: '`email`, `password`  are required' })
        }
        const results = await query(
            `
            SELECT title, name, email FROM l_users WHERE email = ? AND password = ?
            `,
            [email, password]
          )
          return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
    // res.status(200).json({ name: 'John Doe' })
}